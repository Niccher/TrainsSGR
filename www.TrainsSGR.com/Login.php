<html lang="en"><head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!--link rel="icon" href="../../favicon.ico">-->

    <title>SGR Conf</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/Composa.css" rel="stylesheet">

  </head>

  <body>

    <nav class="navbar navbar-default ">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">SGR Conf</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
        </div><!--/.nav-collapse -->
      </div>
    </nav>

    <header id="header">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <h1 class="text-center">Admin Area <small>Account Login</small></h1>
          </div>
        </div>
      </div>

    </header>

    <?php 

include ("Db.php");

session_start();
$_SESSION['errs']="";

  if (isset($_POST['Sbt'])) {

    $eml=strip_tags(($_POST['Eml']));
    $pwd=strip_tags(($_POST['Pwd']));
    
    $_SESSION['emil']=$eml;
    $_SESSION['upass']=$pwd;

      $Comp="SELECT * FROM `tbl_Adminy` WHERE `Email`='$eml' AND `Password`='$pwd' ";
      $Insd=mysqli_query($conni,$Comp) or die("\n".mysqli_error($conni));


      $coun=mysqli_num_rows($Insd);
      $rw=mysqli_fetch_assoc($Insd);
      if ($coun == 1) {
        header("location: index.php");
        echo $Era="Correct<br>";
      }else{
        $Era="Wrong Combination[Please Reconsider Your Inputs]";
        $_SESSION['errs']=$Era; 
      }
  }
  
  ?>


    <section id="main">
      <div class="container">
      <div class="row">
              <div class="col-lg-2">
                <div class="panel panel-danger">
                    <div class="panel-heading">
                      <?php
                      echo $_SESSION['errs'];
                      ?>
                    </div>
                </div>
              </div>

            </div>

        <div class="col-md-4 col-md-offset-4">
          <form id="login" action="Login.php" method="POST" class="well">
            <div class="form-group">
              <label>Email Address</label>
              <input type="text" class="form-control" placeholder="Enter Mail Address" name="Eml">
            </div>
            <div class="form-group">
              <label>Password</label>
              <input type="password" class="form-control" placeholder="Password" name="Pwd">
            </div>
            <input type="submit" class="btn btn-default btn-block" name="Sbt">
          </form>
        </div>
      </div>
      </div>
    </section>

    <footer id="footer">
      <p>Copyright Jubilee Achievements &copy;2017</p>
    </footer>



    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

    <script src="js/bootstrap.min.js"></script>
  
</body></html>
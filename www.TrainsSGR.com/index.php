<html lang="en"><head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!--link rel="icon" href="../../favicon.ico">-->

    <title>SGR Conf</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/Composa.css" rel="stylesheet">

<?php
        include('Db.php');
        session_start();

  $list="SELECT count(*) as total from tbl_Routes";
  $result=mysqli_query($conni,$list);
  $data=mysqli_fetch_assoc($result);

    $list1="SELECT count(*) as total from tbl_Pricing";
  $result1=mysqli_query($conni,$list1);
  $data2=mysqli_fetch_assoc($result1);
?>


  </head>

  <body>

    <nav class="navbar navbar-default ">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">SGR Conf</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="index.php">Dashboard</a></li>
            <li><a href="Routes.php">Routes</a></li>
            <li><a href="Costs.php">Costs</a></li>
          </ul>
          <ul class="nav navbar-nav navbar-right">
            <li ><a href="index.php">Welcome, <?php echo $_SESSION['emil'];?></a></li>
            <li><a href="Login.php">Logout</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>

    <header id="header">
      <div class="container">
        <div class="row">
          <div class="col-md-10">
            <h1><span class="glyphicon glyphicon-cog" aria-hidden="true"></span>Dashboard <small>Manage SGR</small></h1>
          </div>
          <div class="col-md-2">

            <div class="dropdown create">
              <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                Parse Content
                <span class="caret"></span>
              </button>
              <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                <li><a href="Routes.php">Add Route</a></li>
                <li><a href="Costs.php">Manage Cost</a></li>
              </ul>
            </div>

          </div>
        </div>
      </div>

    </header>

    <?php

        if (isset($_SESSION['upass']) && isset($_SESSION['emil'])) {

        }else{
          echo "No Sessions Set By You Man";
          header('Location: Login.php');
        }

    ?>


    <section id="breadcrumb">
      <div class="container">
        <ol class="breadcrumb">
          <li class="active">Dashboard</li>
        </ol>
      </div>
    </section>

    <section id="main">
      <div class="container">
      <div class="row">
        <div class="col-md-3">

          <div class="list-group">
            <a href="index.php" class="list-group-item active main-color-bg">
              <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>&nbsp;&nbsp;Dashboard
            </a>
            <a href="Routes.php" class="list-group-item"><span class="glyphicon glyphicon-road" aria-hidden="true"></span>&nbsp;&nbsp;Manage Routes <span class="badge"><?php echo $data['total']; ?></span> </a>
            <a href="Costs.php" class="list-group-item"><span class="glyphicon glyphicon-euro" aria-hidden="true"></span>&nbsp;&nbsp;Manage Travel Costs  <span class="badge"><?php echo $data2['total']; ?></span></a>
            <a href="Ticketing.php" class="list-group-item"><span class="glyphicon glyphicon-tasks" aria-hidden="true"></span>&nbsp;&nbsp;Ticketing <span class="badge">0</span> </a>
          </div>
        </div>
        <div class="col-md-9">

          <div class="panel panel-default">
            <div class="panel-heading main-color-bg">
              <h3 class="panel-title">Overview</h3>
            </div>
            <div class="panel-body">
              <div class="col-md-3">
                <div class="well dash-box">
                  <h2><span class="glyphicon glyphicon-road" aria-hidden="true"></span>&nbsp;&nbsp;<span class="badge"><?php echo $data['total']; ?></h2>
                  <h4>Routes</h4>
                </div>
              </div>
              <div class="col-md-3">
                <div class="well dash-box">
                  <h2><span class="glyphicon glyphicon-euro" aria-hidden="true"></span>&nbsp;&nbsp;<span class="badge">Pending</span></h2>
                  <h4>Costs</h4>
                </div>
              </div>
              <div class="col-md-3">
                <div class="well dash-box">
                  <h2><span class="glyphicon glyphicon-tasks" aria-hidden="true"></span>&nbsp;&nbsp;<span class="badge">Active</span></h2>
                  <h4>Ticketing</h4>
                </div>
              </div>
              <div class="col-md-3">
                <div class="well dash-box">
                  <h2><span class="glyphicon glyphicon-stats" aria-hidden="true"></span>&nbsp;<span class="badge">Pending</span></h2>
                  <h4>Ratings</h4>
                </div>
              </div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading">
              <h3 class="panel-title">Latest Routes</h3>
            </div>
            <div class="panel-body">
              Panel content
            </div>
          </div>

        </div>
      </div>
      </div>
    </section>

    <footer id="footer">
      <p>Copyright Jubilee Achievements &copy;2017</p>
    </footer>



    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

    <script src="js/bootstrap.min.js"></script>
  
</body></html>
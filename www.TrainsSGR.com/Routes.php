<html lang="en"><head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!--link rel="icon" href="../../favicon.ico">-->

    <title>SGR Conf</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/Composa.css" rel="stylesheet">
    <link href="8assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/css/font-awesome.min.css" rel="stylesheet">
    <link href="assets/css/dataTables.bootstrap.min.css" rel="stylesheet">

        <?php
        include('Db.php');
        session_start();

  $list="SELECT count(*) as total from tbl_Routes";
  $result=mysqli_query($conni,$list);
  $data=mysqli_fetch_assoc($result);

    $list1="SELECT count(*) as total from tbl_Pricing";
  $result1=mysqli_query($conni,$list1);
  $data2=mysqli_fetch_assoc($result1);
    ?>

  </head>

  <body>

    <nav class="navbar navbar-default ">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">SGR Conf</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li ><a href="index.php">Dashboard</a></li>
            <li class="active"><a href="Routes.php">Routes</a></li>
            <li><a href="Costs.php">Costs</a></li>
          </ul>
          <ul class="nav navbar-nav navbar-right">
            <li ><a href="index.php">Welcome, <?php echo $_SESSION['emil'];?></a></li>
            <li><a href="Login.php">Logout</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>

    <header id="header">
      <div class="container">
        <div class="row">
          <div class="col-md-10">
            <h1><span class="glyphicon glyphicon-cog" aria-hidden="true"></span>Dashboard <small>Manage Routes And Paths</small></h1>
          </div>
          <div class="col-md-2">

            <div class="dropdown create">
              <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                Parse Content
                <span class="caret"></span>
              </button>
              <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                <li><a href="Routes.php">Add Route</a></li>
                <li><a href="Costs.php">Manage Cost</a></li>
                <!--<li role="separator" class="divider"></li>
                <li><a href="#">Separated link</a></li>-->
              </ul>
            </div>
          </div>
        </div>
      </div>

    </header>


    <?php

        if (isset($_SESSION['upass']) && isset($_SESSION['emil'])) {
          if (isset($_POST['Sav'])) {

            $rt = $_POST['rut'];
            $dy = $_POST['dy'];
            $dr = $_POST['dr'];
            $srt = $_POST['str'];
            $st1 = $_POST['st1'];
            $st2 = $_POST['st2'];
            $st3 = $_POST['st3'];
            $ds = $_POST['ds'];

            //(`Count`,`Route`, `Days`, `Duration`, `Start`,`StopOver1`, `StopOver2`,`StopOver3`, `Destination`)
        $inject="INSERT INTO `tbl_Routes` (`Route`, `Days`, `Duration`, `Start`,`StopOver1`, `StopOver2`,`StopOver3`, `Destination`) VALUES ('$rt','$dy','$dr','$srt','$st1','$st2','$st3','$ds')";

                    $nig=(mysqli_query($conni,$inject));

                    if($nig === true){
                        header("location:Routes.php");
                    }
                    else{
                      #echo "Insertion Error!";
                        echo mysqli_error($conni);
                    }

          }

        }else{
          echo "No Sessions Set By You Man";
          header('Location: Login.php');
        }

    ?>

    <section id="breadcrumb">
      <div class="container">
        <ol class="breadcrumb">
          <li> <a href="index.php">Dashboard</a></li>
          <li class="active">Routes</li>
        </ol>
      </div>
    </section>

    <section id="main">
      <div class="container">
      <div class="row">
        <div class="col-md-3">

          <div class="list-group">
            <a href="index.php" class="list-group-item ">
              <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>&nbsp;&nbsp;Dashboard
            </a>
            <a href="Routes.php" class="list-group-item main-color-bg"><span class="glyphicon glyphicon-road" aria-hidden="true"></span>&nbsp;&nbsp;Manage Routes <span class="badge"><?php echo $data['total']; ?></span> </a>
            <a href="Costs.php" class="list-group-item"><span class="glyphicon glyphicon-euro" aria-hidden="true"></span>&nbsp;&nbsp;Manage Travel Costs <span class="badge"><?php echo $data2['total']; ?></span> </a>
            <a href="Ticketing.php" class="list-group-item"><span class="glyphicon glyphicon-tasks" aria-hidden="true"></span>&nbsp;&nbsp;Ticketing <span class="badge">0</span> </a>
          </div>
        </div>
        <div class="col-md-9">

          <div class="panel panel-default">
            <div class="panel-heading main-color-bg">
              <h3 class="panel-title">Routes And Paths</h3>
            </div>
            <div class="panel-body">

            <div class="row">
                <div class="col-lg-16">
                        <!-- .panel-heading -->
                        <div class="panel-body">
                            <div class="panel-group" id="accordion">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">Add A Route</a>
                                        </h4>
                                    </div>
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                        <div class="panel-body">
                                            <form method="POST" action="Routes.php" enctype="multipart/form-data">

                          <div class="input-group">
                                  <span class="input-group-addon" id="basic-addon1">Route</span>
                                  <input type="text" class="form-control" name="rut" placeholder="Route" aria-describedby="basic-addon1">
                              </div>
                              <br>
                              <div class="input-group">
                                  <span class="input-group-addon" id="basic-addon1">Days</span>
                                  <input type="text" class="form-control" name="dy" placeholder="Days" aria-describedby="basic-addon1">
                              </div>
                              <br>
                              <div class="input-group">
                                  <span class="input-group-addon" id="basic-addon1">Duration</span>
                                  <input type="text" class="form-control" name="dr" placeholder="Duration" aria-describedby="basic-addon1">
                              </div>
                              <br>
                              <div class="input-group">
                                  <span class="input-group-addon" id="basic-addon1">Start</span>
                                  <input type="text" class="form-control" name="str" placeholder="Start" aria-describedby="basic-addon1">
                              </div>
                              <br>
                              <div class="input-group">
                                  <span class="input-group-addon" id="basic-addon1">StopOver1</span>
                                  <input type="text" class="form-control" name="st1" placeholder="StopOver 1" aria-describedby="basic-addon1">
                              </div>
                              <br>
                              <div class="input-group">
                                  <span class="input-group-addon" id="basic-addon1">StopOver2</span>
                                  <input type="text" class="form-control" name="st2" placeholder="StopOver 2" aria-describedby="basic-addon1">
                              </div>
                              <br>
                              <div class="input-group">
                                  <span class="input-group-addon" id="basic-addon1">StopOver3</span>
                                  <input type="text" class="form-control" name="st3" placeholder="StopOver 3" aria-describedby="basic-addon1">
                              </div>
                              <br><div class="input-group">
                                  <span class="input-group-addon" id="basic-addon1">Destination</span>
                                  <input type="text" class="form-control" name="ds" placeholder="Destination" aria-describedby="basic-addon1">
                              </div>
                              <br>

                              <button type="submit" name="Sav" class="btn btn-warning col-md-3 col-md-offset-3">Insert</button>

                        </form>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">View Routes</a>
                                        </h4>
                                    </div>
                                    <div id="collapseTwo" class="panel-collapse collapse">
                                        <div class="panel-body">
                                                                    <div class="panel-body">
                                

                            <div class="table-responsive">  
                                 <table class="table table-bordered table-striped table-hover">  
                                      <tr>  
                                           <th width="30%">Route</th>  
                                           <th width="10%">Days</th>  
                                           <th width="30%">Duration</th>  
                                           <th width="30%">Start</th>
                                           <th width="30%">StopOver1</th>  
                                           <th width="30%">StopOver2</th>  
                                           <th width="30%">StopOver3</th>  
                                           <th width="30%">Destination</th>
                                           <th width="30%">More</th>
                                      </tr>  
                <?php  
                    //$connect=mysqli_connect('localhost','root','','Muruakey') or die("Wrong Conn  ".mysqli_connect_error()); 
                    $query = "SELECT * FROM `tbl_Routes` ";  
                    $result = mysqli_query($conni, $query);  
                    while($row = mysqli_fetch_array($result))  {  
                ?>  
                                      <tr>  
                                           <td><?php echo $row["Route"]; ?></td>  
                                           <td><?php echo $row["Days"]; ?></td>  
                                           <td><?php echo $row["Duration"]; ?></td>  
                                           <td><?php echo $row["StopOver1"]; ?></td> 
                                           <td><?php echo $row["Start"]; ?></td>  
                                           <td><?php echo $row["StopOver2"]; ?></td>  
                                           <td><?php echo $row["StopOver3"]; ?></td>  
                                           <td><?php echo $row["Destination"]; ?></td>  
                                           <td><input type="button" name="view" value="view" id="<?php echo $row["Id"]; ?>" class="btn btn-info btn-xs view_data" /></td>  
                                      </tr>  
                                      <?php  
                                      }  
                                      ?>  
                                 </table>  
                            </div> 


                        </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- .panel-body -->
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
                </div>


                    </div>
                    <!-- /.panel -->
                </div>

            </div>

          </div>

        </div>
        
      </div>
      </div>
    </section>


    <footer id="footer">
      <p>Copyright Jubilee Achievements &copy;2017</p>
    </footer>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="script.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/jquery.dataTables.min.js"></script>
    <script src="assets/js/dataTables.bootstrap.min.js"></script>
  
</body></html>
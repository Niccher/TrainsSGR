<?php
$dbh = new PDO('mysql:host=localhost;dbname=TrainsSGR', 'root', '');
$page = isset($_GET['p'])? $_GET['p'] : '';
if($page=='add'){
    try{
        $rt = $_POST['rt'];
        $dy = $_POST['dy'];
        $dr = $_POST['dr'];
        $srt = $_POST['str'];
        $st1 = $_POST['st1'];
        $st2 = $_POST['st2'];
        $st3 = $_POST['st3'];
        $ds = $_POST['ds'];
        $stmt = $dbh->prepare("INSERT INTO tbl_Routes VALUES(?,?,?,?,?,?,?,?)");
        $stmt->bindParam(1,$rt);
        $stmt->bindParam(2,$dy);
        $stmt->bindParam(3,$dr);
        $stmt->bindParam(4,$srt);
        $stmt->bindParam(5,$st1);
        $stmt->bindParam(6,$st2);
        $stmt->bindParam(7,$st3);
        $stmt->bindParam(8,$ds);
        if($stmt->execute()){
            print "<div class='alert alert-success' role='alert'>Data has been added</div>";
        } else{
            print "<div class='alert alert-danger' role='alert'>Failed to add data</div>";
        }
    } catch(PDOException $e){
        print "Error!: " . $e->getMessage() . "<br/>";
    } 
} /*else if($page=='update'){
    try{
        $rt = $_POST['rt'];
        $dy = $_POST['dy'];
        $dr = $_POST['dr'];
        $srt = $_POST['str'];
        $st1 = $_POST['st1'];
        $st2 = $_POST['st2'];
        $st3 = $_POST['st3'];
        $ds = $_POST['des'];
        $stmt = $dbh->prepare("UPDATE tbl_Routes SET name=?, email=?, phone=?, address=? WHERE id=?");
        $stmt->bindParam(1,$rt);
        $stmt->bindParam(2,$dy);
        $stmt->bindParam(3,$dr);
        $stmt->bindParam(4,$srt);
        $stmt->bindParam(5,$st1);
        $stmt->bindParam(6,$st2);
        $stmt->bindParam(7,$st3);
        $stmt->bindParam(8,$ds);
        if($stmt->execute()){
            print "<div class='alert alert-success' role='alert'>Data has been updated</div>";
        } else{
            print "<div class='alert alert-danger' role='alert'>Failed to update data</div>";
        }
    } catch(PDOException $e){
        print "Error!: " . $e->getMessage() . "<br/>";
    } 
} else if($page=='delete'){
    try{
        $id = $_POST['id'];
        $stmt = $dbh->prepare("DELETE FROM tbl_Routes WHERE id=?");
        $stmt->bindParam(1,$id);
        if($stmt->execute()){
            print "<div class='alert alert-success' role='alert'>Data has been deleted</div>";
        } else{
            print "<div class='alert alert-danger' role='alert'>Failed to delete data</div>";
        }
    } catch(PDOException $e){
        print "Error!: " . $e->getMessage() . "<br/>";
    } 
} else{
    try{
        $stmt = $dbh->prepare("SELECT * FROM tbl_Routes ORDER BY id DESC");
        $stmt->execute();
        while($row = $stmt->fetch()){
            print "<tr>";
            print "<td>".$row['id']."</td>";
            print "<td>".$row['name']."</td>";
            print "<td>".$row['email']."</td>";
            print "<td>".$row['phone']."</td>";
            print "<td>".$row['address']."</td>";
            print "<td class='text-center'><div class='btn-group' role='group' aria-label='group-".$row['id']."'>";
            ?> 
            <button onclick="editData('<?php echo $row['id'] ?>','<?php echo $row['name'] ?>','<?php echo $row['email'] ?>','<?php echo $row['phone'] ?>','<?php echo $row['address'] ?>')" class='btn btn-warning'>Edit</button>
            <button onclick="removeConfirm('<?php echo $row['id'] ?>')" class='btn btn-danger'>Trash</button>
            <?php 
            print "</div></td>";
            print "</tr>";
        }
    } catch(PDOException $e){
        print "Error!: " . $e->getMessage() . "<br/>";
    }
}*/
?>
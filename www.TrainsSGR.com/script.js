$('table').dataTable();
viewData();
$('#update').prop("disabled",true);

function viewData(){
    $.get('server.php', function(data){
        $('tbody').html(data)
    })
}

function saveData(){
    var rt = $('#rt').val()
    var dy = $('#dy').val()
    var dr = $('#dr').val()
    var srt = $('#srt').val()
    var st1 = $('#st1').val()
    var st2 = $('#st2').val()
    var st3 = $('#st3').val()
    var ds = $('#ds').val()
    $.post('server.php?p=add', {rt:rt, dy:dy, dr:dr, str1:str1,str2:str2,str3:str3, ds:ds}, function(data){
        viewData()
        $('#rt').val()
        $('#dy').val(' ')
        $('#dr').val(' ')
        $('#srt').val(' ')
        $('#st1').val(' ')
        $('#st2').val(' ')
        $('#st3').val(' ')
        $('#ds').val(' ')
    })
}

function editData(id, nm, em, hp, ad) {
    $('#id').val(id)
    $('#nm').val(nm)
    $('#em').val(em)
    $('#hp').val(hp)
    $('#ad').val(ad)
    $('#id').prop("readonly",true);
    $('#save').prop("disabled",true);
    $('#update').prop("disabled",false);
}

function updateData(){
    var id = $('#id').val()
    var name = $('#nm').val()
    var email = $('#em').val()
    var phone = $('#hp').val()
    var address = $('#ad').val()
    $.post('server.php?p=update', {id:id, nm:name, em:email, hp:phone, ad:address}, function(data){
        viewData()
        $('#id').val(' ')
        $('#nm').val(' ')
        $('#em').val(' ')
        $('#hp').val(' ')
        $('#ad').val(' ')
        $('#id').prop("readonly",false);
        $('#save').prop("disabled",false);
        $('#update').prop("disabled",true);
    })
}

function deleteData(id){
    $.post('server.php?p=delete', {id:id}, function(data){
        viewData()
    })
}

function removeConfirm(id){
    var con = confirm('Are you sure, want to delete this data!');
    if(con=='1'){
        deleteData(id);
    }
}

$(function() {

    var $sidebar   = $("#sidebar"), 
        $window    = $(window),
        offset     = $sidebar.offset(),
        topPadding = 15;

    $window.scroll(function() {
        if ($window.scrollTop() > offset.top) {
            $sidebar.stop().animate({
                marginTop: $window.scrollTop() - offset.top + topPadding
            });
        } else {
            $sidebar.stop().animate({
                marginTop: 0
            });
        }
    });
    
});